package ru.potapov.hw1.task2;

/**В первый день спортсмен пробежал 10 км. Каждый следующий день он пробегал на 10% больше, чем в предыдущий.
 Определить:
 - сколько км он пробежит в 10-й день;
 - в какой день он впервые пробежит больше 20 км;
 - в какой день его суммарный пробег превысит 100 км.**/

public class Task2 {
    public static void main(String[] args) {
        double firstDayDistance = 10;

        System.out.println("На 10 день пробежит: " + calculateDistance10(firstDayDistance));
        System.out.println("======");
        System.out.println("Впервые пробежит 20 км на " + calculateDistance20(firstDayDistance) + " день");
        System.out.println("======");
        System.out.println("Суммарный пробег превысит 100км на " + calculateDistance100(firstDayDistance) + " день");
    }

    private static double calculateDistance10(double dayDistance) {
        int day = 1;
        do {
            day++;
            dayDistance *= 1.1;
            System.err.println("SD: " + dayDistance + " " + " day: " +day);
        } while (day < 10);
        return dayDistance;
    }

    private static int calculateDistance20 (double dayDistance) {
        int day = 1;
        do {
            dayDistance *= 1.1;
            day++;
        } while (dayDistance < 20);
        return day;
    }

    private static int calculateDistance100(double dayDistance) {
        int day = 0;
        double total = 0;
        do {
            total += dayDistance;
            dayDistance *= 1.1;
            day++;
        } while (total < 100);
        return day;
    }
}
