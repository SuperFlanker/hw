package ru.potapov.hw1.task5;

/*
Написать программу, которая считывает с клавиатуры координаты 2х точек на плоскости и ищет расстояние между ними.
 Необходимо выделить необходимые для решения задачи абстракции (классы).
 */

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task5 {
    public static void main(String[] args) {
        System.out.println("Введите координаты 1 точки:");
        Coordinates coordinates1 = readCoordinates();
        System.out.println("Введите координаты 2 точки:");
        Coordinates coordinates2 = readCoordinates();
        System.out.println(coordinates1);
        System.out.println(coordinates2);
        System.out.println("Расстояние между точками: " + calculateDistance(coordinates1, coordinates2));
    }

    private static Coordinates readCoordinates () {
        int x, y;
        Scanner scanner = new Scanner(System.in);
        do {
            try {
                System.out.println("Введите x:");
                x = scanner.nextInt();
                System.out.println("Введите y:");
                y = scanner.nextInt();
                return new Coordinates(x, y);
            } catch (RuntimeException r) {
                System.err.println("Error");
            }
        } while (true);
    }

    private static double calculateDistance (Coordinates coordinates1, Coordinates coordinates2) {
        double deltaX = pow((coordinates2.getX() - coordinates1.getX()), 2);
        double deltaY = pow((coordinates2.getY() - coordinates1.getY()), 2);
        return sqrt(deltaX + deltaY);
    }

}
