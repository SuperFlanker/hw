package ru.potapov.hw1.task1;


/**Ввести с клавиатуры дату в формате дд.мм.гггг, вычислить следующую дату
 и вывести на экран в том же формате (не использовать DateTime API)**/

import java.util.Scanner;


public class Task1 {
    private static Scanner scanner = new Scanner(System.in);
    private static int[] maxDay = {31,28,31,30,31,30,31,31,30,31,30,31};
    public static void main(String[] args) {
        Date date = readDate();
        Date newDate = calculateDate(date);
        System.out.println(date.getDay() + "." + date.getMonth() + "." + date.getYear());
        System.out.println(newDate.getDay() + "." + newDate.getMonth() + "." + newDate.getYear());
        System.out.println(date);
        System.out.println(newDate);
    }

    private static Date readDate() {
        do {
            try {
                System.out.println("Ведите день");
                int day = scanner.nextInt();
                System.out.println("Введите месяц");
                int month = scanner.nextInt();
                System.out.println("Введите год");
                int year = scanner.nextInt();
                Date date = new Date(day, month, year);
                if (validate(date)) {
                    return date;
                }
            } catch (RuntimeException e) {
                System.err.println("Ошибка ввода.");
            }
        } while (true);
    }

    private static int[] getMaxDayCount(int year) {
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
            maxDay[1] = 29;
        }
        return maxDay;
    }

    private static boolean validate(Date date) {
        int day = date.getDay();
        int month = date.getMonth();
        int year = date.getYear();
        if (day > 31 || day > maxDay[month - 1] || day < 1 || month > 12 || month < 0 || year < 0) {
            System.err.println("Неверный формат");
            return false;
        }
        return true;
    }

    private static Date calculateDate(Date date) {
        int day = date.getDay();
        int month = date.getMonth();
        int year = date.getYear();
        int [] maxDay = getMaxDayCount(year);
        if (day == maxDay[month - 1]){
            day = 1;
            if (month == 12){
                month = 1;
                year++;
            } else {
                month++;
            }
        } else {
            day++;
        }
        return new Date(day, month, year);
    }
}