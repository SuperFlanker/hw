package ru.potapov.hw1.task3;

/*
 * Игра «куча». Человек и компьютер по очереди делают ходы, забирая из кучи, состоящей из N предметов, от 1 до K штук.
 * Проигрывает тот, кто забрал последний предмет. Найти для компьютера оптимальную стратегию.
 */

import java.util.Scanner;

public class Task3 {
    private static Scanner scanner = new Scanner(System.in);
    private static boolean criteria;
    private static boolean isHumanStage = true;

    public static void main(String[] args) {
        System.out.println("Введите количество предметов в куче: ");
        int totalCount = scanner.nextInt();
        System.out.println("В куче " + totalCount + " предметов.");
        System.out.println("Введите максимальное вычитаемое число:");
        int max = scanner.nextInt();
        subtractionFromHeap(max, totalCount);
    }

    private static void subtractionFromHeap(int max, int totalCount) {
        do {
            if (isHumanStage) {
                System.out.println("Ход человека: ");
                totalCount -= moveHuman(max, totalCount);
                criteria = true;
            } else {
                totalCount -= calculateSubtractionComputer(totalCount, max);
                criteria = false;
            }
            if (totalCount == 1) {
                System.out.println("---");
                break;
            } else {
                System.out.println("В куче осталось: " + totalCount + " предметов");
                isHumanStage = !isHumanStage;
            }
        } while (true);
        checkingWinner();
    }

    private static void checkingWinner() {
        if (criteria) {
            System.out.println("Победил человек!");
        } else {
            System.out.println("Победил компьютер!");
        }
    }

    private static int moveHuman(int max, int totalCount) {
        int moveHumanValue;
        do {
            try {
                moveHumanValue = scanner.nextInt();
                if (moveHumanValue <= max && moveHumanValue > 0 && (totalCount - moveHumanValue) >= 0) {
                    return moveHumanValue;
                } else {
                    System.err.println("ERROR");
                }
            } catch (RuntimeException e) {
                System.err.println("Неверный формат");
            }
        } while (true);
    }

    private static int calculateSubtractionComputer(int totalCount, int max) {
        int subtractionComputer;
        if (totalCount - 1 <= max) {
            subtractionComputer = totalCount - 1;
            System.out.println("Ход компьютера (1): " + subtractionComputer);
        } else if ( ((totalCount - 1) % max) % 2 == 0 && (totalCount - 1) / max <= max) {
            subtractionComputer = (totalCount - 1) / max;
            System.out.println("Ход компьютера (2): " + subtractionComputer);
        } else {
            subtractionComputer = 1;
            System.out.println("Ход компьютера (3): " + subtractionComputer);
        }
        return subtractionComputer;
    }
}