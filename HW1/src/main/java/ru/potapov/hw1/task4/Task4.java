package ru.potapov.hw1.task4;

/*
Написать программу, которая решает квадратное уравнение. Коэффициенты a, b и c вводятся с клавиатуры.
Необходимо выделить необходимые для решения задачи абстракции (классы).
 */
import java.util.Scanner;

public class Task4 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Coefficients coefficients = readCoefficients();
        System.out.println(coefficients);
        calculateX(coefficients, discriminant(coefficients));
    }

    private static Coefficients readCoefficients () {
        double a = 0;
        double b = 0;
        double c = 0;
        do {
            try {
                System.out.println("Введите коэффициент a: ");
                a = scanner.nextDouble();
                if (a == 0) {
                    System.err.println("A не может быть равным 0");
                    continue;
                }
                System.out.println("Введите коэффициент b: ");
                b = scanner.nextDouble();
                System.out.println("Введите коэффициент c: ");
                c = scanner.nextDouble();
                return new Coefficients(a, b, c);
            } catch (Exception e) {
            }
        } while (true);
    }
    private static double discriminant (Coefficients coefficients) {
        return Math.pow(coefficients.getB(), 2) - 4 * coefficients.getA() * coefficients.getC();
    }

    private static void calculateX(Coefficients coefficients, double discriminant) {
        double x1, x2;
        if (discriminant > 0) {
            x1 = (-coefficients.getB() + Math.sqrt(discriminant)) / (2 * coefficients.getA());
            x2 = (-coefficients.getB() - Math.sqrt(discriminant)) / (2 * coefficients.getA());
            System.out.println("x1 = " + x1 + " x2 = " + x2);
        } else if (discriminant == 0) {
            x1 = ( -coefficients.getB() + Math.sqrt(discriminant)) / (2 * coefficients.getA());
            System.out.println("x = " + x1);
        } else {
            System.err.println("Решений нет");
        }
    }
}
