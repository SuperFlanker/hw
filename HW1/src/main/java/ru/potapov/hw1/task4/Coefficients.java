package ru.potapov.hw1.task4;

class Coefficients {

    private double a;
    private double b;
    private double c;

    public Coefficients(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    @Override
    public String toString() {
        return "Coefficients{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
